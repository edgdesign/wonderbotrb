module Wonderbot
	module Commands
		extend Discordrb::EventContainer
		message(start_with: '!commands') do |msg|
			msg.respond <<-TEXT
!ridethebus
!ridethebus leaderboard
!ridethebus stats

!boof stats
!boof <optional emoji>
!boofassist <@mention> // assist a users boof

!everyfartisashart
!sex
!seek
!serious
!shouldi <question>
!ascii <text>
!woke
!joke
!bass
!trogdor
TEXT
		end
	end
end