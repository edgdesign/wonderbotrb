require 'cards'

module Wonderbot
	module RideTheBus
		extend Discordrb::EventContainer

		class Stats < ActiveRecord::Base
			self.table_name = 'ride_the_bus_stats'
			self.primary_key = :id

			def readonly?
				true
			end

			class << self
				def for_user user_id
					stat = find(user_id)
					response = ""
					response << "You have won %#{stat.rate.round(2)} of your games.\n"
					response << "Won: #{stat.wins}, lost: #{stat.losses}, total: #{stat.games}"
					response

				rescue ActiveRecord::RecordNotFound
					"You havent played any games with wonderbot :(\n"
				end

				def bot_win_rate
					(pluck(:losses).sum.to_f / pluck(:games).sum.to_f * 100).round(2)
				end

				def leaderboard
					members = Wonderbot.members
					Stats.all.order(rate: :desc).map do |stat|
						user = members.find {|m| m["user"]["id"] == stat.id }

						pp user

						"#{user['user']['username']} -- rate: #{stat.rate.round(2)}\n"

					end.join
				end
			end
		end

		class Game < ActiveRecord::Base
			self.table_name = 'ride_the_bus_games'
		end


		message(start_with: '!ridethebus leaderboard') do |msg|
			msg.respond Stats.leaderboard
		end

		message(start_with: '!ridethebus stats') do |msg|
			msg.respond "Wonderbot has won %#{Stats.bot_win_rate} of games \n #{Stats.for_user(msg.user.id)}"
		end

		message(start_with: '!ridethebus', end_with: '!ridethebus') do |msg|

			game = ::Cards::RideTheBus.new

			response = "Lets play ride the bus! type <black|red|above|below|inside|outside> to play \n"
			response << "#{game.turn.to_s.gsub('_', ' ')}?"
			msg.respond response


			msg.user.await :"guess_#{msg.author.id}" do |guess_event|

				guess = guess_event.message.content.downcase

				unless guess =~ /^(black|red|above|below|inside|outside)$/
					guess_event.respond 'type <black|red|above|below|inside|outside> to play'
					return false
				end

				game.play guess

				if game.complete?
					Game.create(
						won: game.won?,
						user_id: msg.user.id
					)
				end

				response = ["```\n", Cards.print_ascii(game.cards), "```\n"].join
				response << "Good Guess\n" if game.won?
				response << "You win!\n" if game.complete? and game.won?
				response << "You lose! Drink!\n" if game.complete? and !game.won?
				response << "#{game.turn.to_s.gsub('_', ' ')}? \n" unless game.complete?
				response << Stats.for_user(msg.user.id) if game.complete?
				
				guess_event.respond(response).tap do |r|
					if game.complete? and game.won? 
						r.create_reaction msg.server.emoji[754156041981263982].to_reaction
					end
				end

				# guess_event.respond("turn #{game.turn} won? #{game.won?} complete? #{game.complete?}")

				game.complete?
			end
		end
	end
end
