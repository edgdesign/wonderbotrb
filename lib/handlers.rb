require 'net/http'
require 'json'
require 'artii'

module Wonderbot
	module Handlers
		extend Discordrb::EventContainer

		message(start_with: '!everyfartisashart') { |msg| msg.respond '<3'}
		message(start_with: '!sex') { |msg| msg.respond 'Hey daddy'}
		message(start_with: '!seek') { |msg| msg.respond 'dubs!'}
		message(start_with: '!serious') { |msg| msg.respond 'love'}
		message(start_with: '!shouldi') { |msg| msg.respond ['hell yes', 'yes', 'no', 'hell no'].sample }


		message(start_with: '!wonderbread') do |msg| 

			msg.respond msg.server.emoji[754156041981263982].mention

		end

		message(start_with: '!ascii') do |msg|
			content = msg.content.gsub('!ascii', '')
			content = ["```", Artii::Base.new(font: 'slant').asciify(content), "```"].join

			msg.respond content
		end

		message(start_with: '!woke') do |msg|
			res = Net::HTTP.get(URI('http://hippie.edgdesign.info/speak'))
			msg.respond(res)
		end

		message(start_with: '!joke') do |msg|
			res = Net::HTTP.get(URI('https://sv443.net/jokeapi/v2/joke/Any?type=single'))
			res = JSON.parse(res)
			msg.respond(res['joke'])
		end

		message(start_with: '!bass') do |msg|
			bass = <<-TEXT
▒█▀▀▄ █▀▀█ █▀▀█ █▀▀█
▒█░▒█ █▄▄▀ █░░█ █░░█
▒█▄▄▀ ▀░▀▀ ▀▀▀▀ █▀▀▀

▀▀█▀▀ █░░█ █▀▀
░▒█░░ █▀▀█ █▀▀
░▒█░░ ▀░░▀ ▀▀▀

▒█▀▀█ █▀▀█ █▀▀ █▀▀
▒█▀▀▄ █▄▄█ ▀▀█ ▀▀█
▒█▄▄█ ▀░░▀ ▀▀▀ ▀▀▀
TEXT

			msg.respond(bass)
		end

		message(start_with: '!trogdor') do |msg|

			trogdor = <<-'TEXT'
                                                 :::
                                             :: :::.
                       \/,                    .:::::
           \),          \`-._                 :::888
           /\            \   `-.             ::88888
          /  \            | .(                ::88
         /,.  \           ; ( `              .:8888
            ), \         / ;``               :::888
           /_   \     __/_(_                  :88
             `. ,`..-'      `-._    \  /      :8
               )__ `.           `._ .\/.
              /   `. `             `-._______m         _,
  ,-=====-.-;'                 ,  ___________/ _,-_,'"`/__,-.
 C   =--   ;                   `.`._    V V V       -=-'"#==-._
:,  \     ,|      UuUu _,......__   `-.__A_A_ -. ._ ,--._ ",`` `-
||  |`---' :    uUuUu,'          `'--...____/   `" `".   `
|`  :       \   UuUu:
:  /         \   UuUu`-._
 \(_          `._  uUuUu `-.
 (_3             `._  uUu   `._
                    ``-._      `.
                         `-._    `.
                             `.    \
                               )   ;
                              /   /
               `.        |\ ,'   /
                 ",_A_/\-| `   ,'
                   `--..,_|_,-'\
                          |     \
                          |      \__
                          |__
TEXT
	
			msg.respond ['```', trogdor, '```'].join
		end
	end
end
