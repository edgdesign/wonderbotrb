require 'redis'

module Wonderbot
	module Boof

		class Boofing < ActiveRecord::Base; end

		extend Discordrb::EventContainer

		redis ||= Redis.new

		message(start_with: '!boof stats', end_with: 'stats') do |msg|
			msg << "You have boofed #{Boofing.where(user_id: msg.author.id).count} times"
			msg << "You have boof assisted #{Boofing.where(assist_id: msg.author.id).count} times"
		end

		message(start_with: '!boofassist') do |msg|
			author = msg.author

			msg.message.mentions.each do |mention|
				Discordrb::LOGGER.info mention.id
				boofing = Boofing.where(user_id: mention.id)
												 .order(created_at: :desc)
											 	 .limit(1)
											 	 .take

				if boofing && !boofing.assist_id.present?
					boofing.update ({
						assist_id: author.id
					})

					msg << "#{author.mention} boof assisted <@#{boofing.user_id}>"
				end
			end
		end

		message(start_with: /\!boof(?!(\sstats|assist))/) do |msg|
			boof_count_key ='wk:boof:count'
			boof_list_key = 'wk:boof:list'

			t = Time.now.getlocal('-07:00')
			t = Time.new(t.year, t.month, t.day, 23, 59, 59, '-07:00').to_i

			redis.setnx boof_list_key, ''
			redis.setnx boof_count_key, 0
			
			redis.expireat boof_list_key, t
			redis.expireat boof_count_key, t #expireat takes a UNIX epoch timestamp

			redis.append boof_list_key, msg.content.gsub('!boof', '').strip

			list = redis.get boof_list_key
			count = redis.incr boof_count_key

			Boofing.create({
				user_id: msg.user.id
			})

			msg << "wonderkids have boofed #{count} times today:"
			msg << list
		end
	end
end
