	class CreateStatsView < ActiveRecord::Migration[5.2]
		def self.up
			execute <<~SQL
				CREATE VIEW IF NOT EXISTS ride_the_bus_stats AS
					WITH
					Wins as (
						SELECT
							g1.user_id as user_id,
							COUNT(g1.user_id) as win_count
						FROM ride_the_bus_games g1
						WHERE g1.won = 1
						GROUP BY g1.user_id
					),
					Losses as (
						SELECT
							g2.user_id as user_id,
							COUNT(g2.user_id) as loss_count
						FROM ride_the_bus_games g2
						WHERE g2.won = 0
						GROUP BY g2.user_id
					)
					SELECT
						g.user_id as id,
						COUNT(g.user_id) as games,
						COALESCE(win_count, 0.0) as wins,
						loss_count as losses,
						(cast(COALESCE(win_count, 0) as float) / cast(COUNT(g.user_id) as float)) * 100 as rate
					FROM
						ride_the_bus_games g
					LEFT JOIN Wins ON Wins.user_id = g.user_id
					LEFT JOIN Losses ON Losses.user_id = g.user_id
					GROUP BY g.user_id
			SQL
		end
		def self.down
			execute <<~SQL
				DROP VIEW IF EXISTS ride_the_bus_stats;
			SQL
		end
	end