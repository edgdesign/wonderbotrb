	class CreateBoofings < ActiveRecord::Migration[5.2]
		def change
			create_table :boofings do |t|
				t.string :user_id, index: true
				t.string :assist_id, index: true
				t.timestamps
			end
			add_index :boofings, [:user_id, :created_at]
		end
	end
