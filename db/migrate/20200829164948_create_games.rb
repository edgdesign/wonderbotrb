class CreateGames < ActiveRecord::Migration[5.2]
  def self.up
  	create_table :ride_the_bus_games do |t|
  		t.boolean :won, null: false, index: true
  		t.string :user_id, index: true
  		t.timestamps
  	end
  end
  def self.down
  	drop_table :ride_the_bus_games
  end
end
