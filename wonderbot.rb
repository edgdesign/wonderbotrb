require 'bundler/setup'
require 'discordrb'
require 'yaml'
require 'active_record'

Dir[File.join(__dir__, 'lib', '*.rb')].each { |file| require file }

db_config = YAML::load_file 'db/config.yml'

ActiveRecord::Base.establish_connection db_config[ENV['ENV'] || 'development']

module Wonderbot
	
	cattr_reader :config, :wk_server_id, :api_token
	@@config = YAML::load_file 'config.yml'
	
	@@api_token = "Bot #{@@config['token']}"
	@@wk_server_id = "717970352411770901"


	class << self
		def run
			bot = Discordrb::Bot.new token: @@config['token']

			bot.include! Handlers
			bot.include! Boof
			bot.include! RideTheBus
			bot.include! Commands

			bot.run
		end

		def members
			res = Discordrb::API::Server.resolve_members api_token, wk_server_id, 1000
			JSON.parse res.body
		end
	end
end

